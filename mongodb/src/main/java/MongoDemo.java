import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

public class MongoDemo {

	public static void main(String[] args) throws Exception {

		// connect to the local database server
		Mongo m = new Mongo();

		// get handle to "mydb"
		DB db = m.getDB("hands-on-nosql");

		// Authenticate - optional
		// boolean auth = db.authenticate("foo", "bar");

		// get a collection object to work with
		DBCollection coll = db.getCollection("movieCollection");

		// drop all the data in it
		coll.drop();

		// make a document and insert it
		DBObject movie1 = createMovie("Pirates of Silicon Valley", 1999, new String[] {"Anthony Michael Hall" , "Noah Wyle" , "Joey Slotnick" , "J.G. Hertzler"},8.0, 8474);
		coll.insert(movie1);
		DBObject movie2 = createMovie("The Social Network", 2010, new String[] {"Jesse Eisenberg" , "Andrew Garfield" , "Justin Timberlake" , "Rooney Mara"},7.4, 2345);
		coll.insert(movie2);
		DBObject movie3 = createMovie("The Hitchhiker's Guide to the Galaxy", 2005, new String[] {"Martin Freeman" , "Mos Def" , "Sam Rockwell" , "Zooey Deschanel"},6.7, 544);
		coll.insert(movie3);

		// lets get all the documents in the collection and print them out
		System.out.println("Get all documents: Example 1");
		DBCursor cur = coll.find();
		while (cur.hasNext()) {
			System.out.println(cur.next());
		}
		System.out.println();

		// now use a query to get 1 document out
		System.out.println("Query for a specific movie: Example 2");
		BasicDBObject query = new BasicDBObject();
		query.put("Title", "The Social Network");
		cur = coll.find(query);

		while (cur.hasNext()) {
			System.out.println(cur.next());
		}
		System.out.println();

		// now use a range query to get a larger subset
		System.out.println("All movies made after the year 2000: Example 3");
		query = new BasicDBObject();
		query.put("Year", new BasicDBObject("$gt", 2000));
		cur = coll.find(query);

		while (cur.hasNext()) {
			System.out.println(cur.next());
		}
		System.out.println();

		// range query with multiple constraints
		System.out.println("All movies with score > 7 and <= 8 and at > 3000 votes: Example 4");
		query = new BasicDBObject();
		query.put("Ratings.Score", new BasicDBObject("$gt", 7).append("$lte", 8));
		query.put("Ratings.Votes", new BasicDBObject("$gt", 3000));
		cur = coll.find(query);

		while (cur.hasNext()) {
			System.out.println(cur.next());
		}
		System.out.println();

		// It is possible to add indexes to your collection for faster searching on big data sets

		// create an index on the "Year" field
		coll.createIndex(new BasicDBObject("Year", 1)); // create index on "Year", ascending

		// list the indexes on the collection
		System.out.println("Indexes on current collection: Bonus Example");
		List<DBObject> list = coll.getIndexInfo();
		for (DBObject o : list) {
			System.out.println(o);
		}
	}

	private static BasicDBObject createMovie(String title, int year, String[] actors, double score, int votes) {
		BasicDBObject movieDoc = new BasicDBObject();

		movieDoc.put("Title", title);
		movieDoc.put("Year", year);
		movieDoc.put("Actors", actors);

		BasicDBObject ratingsDoc = new BasicDBObject();

		ratingsDoc.put("Score", score);
		ratingsDoc.put("Votes", votes);

		movieDoc.put("Ratings", ratingsDoc);

		return movieDoc;
	}
}
