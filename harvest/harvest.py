#!/usr/bin/env python

import json, time, urllib, os.path

def get_user_timeline(user_id, screen_name):
    timeline_str = fetch_data("https://api.twitter.com/1/statuses/user_timeline.json?screen_name=%s&include_entities=1" % screen_name, "statuses/%s.json" % user_id)

def get_user(user_id):
    user_str = fetch_data("https://api.twitter.com/1/users/show.json?user_id=%s" % user_id, "user/%s.json" % user_id)

    d = json.loads(user_str)
    if 'screen_name' in d:
        return d['screen_name']
    else:
        return None

def get_followers(user_id):
    followers_str = fetch_data("https://api.twitter.com/1/followers/ids.json?user_id=%s" % user_id, "followers/%s.json" % user_id)

    followers = json.loads(followers_str)
    return followers

def fetch_data(url, filename):
    if os.path.exists(filename):
        print "reading %s from cache" % filename
        data = read_data(filename)
    else:
        print "reading %s from web" % url
        data = fetch_url(url)
        save_data(filename, data)
        time.sleep(30)
    return data

def fetch_url(url):
    try:
        f = urllib.urlopen(url)
        data = f.read()
        f.close()
    except IOError:
        print "Error getting url %s" % url
        data = ""
    return data

def save_data(filename, data):
    f = open(filename, 'w')
    f.write(data)
    f.close()

def read_data(filename):
    f = open(filename, 'r')
    data = f.read()
    f.close()
    return data

def harvest(user_id):
    followers = get_followers(user_id)
    for follower in followers:
        name = get_user(follower)
        if name is not None:
            timeline = get_user_timeline(follower, name)
        print follower, name

harvest(14352528)
