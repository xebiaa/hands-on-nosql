package com.xebia.nosql.hbase;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;

public class HBaseExample {
	private static final String VALUE_QUALIFIER = "VALUE";
	private static final byte[] VALUE_QUALIFIER_BYTES = Bytes.toBytes(VALUE_QUALIFIER);
	
	private static final String DAY_QUALIFIER = "DAY";
	private static final byte[] DAY_QUALIFIER_BYTES = Bytes.toBytes(DAY_QUALIFIER);
	
	private static final String FAMILY_NAME = "DATA";
	private static final byte[] FAMILY_NAME_BYTES = Bytes.toBytes(FAMILY_NAME);
	
	private static final String TABLE_NAME = "TEST_TABLE";
	
	//Used for formatting row keys
	private final DateFormat keyFormat = new SimpleDateFormat("yyyyMMdd");
	private final DateFormat dayFormat = new SimpleDateFormat("EEEE");
	
	
	public static void main(String[] args) throws Exception {
		new HBaseExample().runExample();
	}
	
	public void runExample() throws Exception {
		/*
		 * All HBase access requires a Configuration instance that hold info on
		 * where the server lives
		 */
		Configuration conf = HBaseConfiguration.create();
		//CHANGE THIS TO HBASE/ZOOKEEPER SERVER
		conf.setStrings("hbase.zookeeper.quorum", "localhost");
		
		//Create a table
		dropAndCreateHbaseTable(conf);
		
		//Create a table connection
		HTable table = openTable(conf);
		
		//Put data in there
		System.out.println("________________ putData() ________________");
		putData(table);
		
		System.out.println("________________ getAndPrintData() ________________");
		getAndPrintData(table);
		
		System.out.println("________________ scanAndPrintData() ________________");
		scanAndPrintData(table);
		
		System.out.println("________________ scanAndPrintDataWithFilter() ________________");
		scanAndPrintDataWithFilter(table);
	}

	private void scanAndPrintDataWithFilter(HTable table) throws IOException {
		//create a scan with a key range
		Date startDate = new Date(System.currentTimeMillis() + 3L * 7 * 24 * 3600000); //three weeks from now
		Date endDate = new Date(System.currentTimeMillis() + 5L * 7 * 24 * 3600000); //five weeks from now
		
		//create a scan object
		Scan scan = new Scan(
				Bytes.toBytes(keyFormat.format(startDate)), //the start row, as bytes; start row is INCLUSIVE
				Bytes.toBytes(keyFormat.format(endDate))); //the end row, as bytes; end row is EXCLUSIVE
		
		//create a filter
		SingleColumnValueFilter filter = new SingleColumnValueFilter(
				FAMILY_NAME_BYTES, 
				VALUE_QUALIFIER_BYTES, 
				CompareOp.GREATER,
				Bytes.toBytes(8952719997152152351L));
		
		scan.setFilter(filter);
		
		//get the scanner
		ResultScanner scanner = table.getScanner(scan);
		
		//iterate results
		Iterator<Result> itr = scanner.iterator();
		while (itr.hasNext()) {
			Result result = itr.next();
			
			String rowKeyString = Bytes.toString(result.getRow()); //getRow() returns the row key
			String dayString = Bytes.toString(result.getValue(FAMILY_NAME_BYTES, DAY_QUALIFIER_BYTES));
			long value = Bytes.toLong(result.getValue(FAMILY_NAME_BYTES, VALUE_QUALIFIER_BYTES));
			
			System.out.println("key = " + rowKeyString + "; day = '" + dayString + "'; value = " + value);
		}
	}
	
	private void getAndPrintData(HTable table) throws IOException {
		//construct a key to GET
		Date someDate = new Date(System.currentTimeMillis() + 3L * 7 * 24 * 3600000); //three weeks from now
		
		//create a get object
		Get get = new Get(
				Bytes.toBytes(keyFormat.format(someDate))); //this will get exactly this row key
		
		//perform the get operation
		Result result = table.get(get);
		
		//fetch results
		String rowKeyString = Bytes.toString(result.getRow()); //getRow() returns the row key
		String dayString = Bytes.toString(result.getValue(FAMILY_NAME_BYTES, DAY_QUALIFIER_BYTES));
		long value = Bytes.toLong(result.getValue(FAMILY_NAME_BYTES, VALUE_QUALIFIER_BYTES));
		
		//print
		System.out.println("key = " + rowKeyString + "; day = '" + dayString + "'; value = " + value);
	}

	private void scanAndPrintData(HTable table) throws IOException {
		//create a scan with a key range
		Date startDate = new Date(System.currentTimeMillis() + 3L * 7 * 24 * 3600000); //three weeks from now
		Date endDate = new Date(System.currentTimeMillis() + 5L * 7 * 24 * 3600000); //five weeks from now
		
		//create a scan object
		Scan scan = new Scan(
				Bytes.toBytes(keyFormat.format(startDate)), //the start row, as bytes; start row is INCLUSIVE
				Bytes.toBytes(keyFormat.format(endDate))); //the end row, as bytes; end row is EXCLUSIVE
		
		//get the scanner
		ResultScanner scanner = table.getScanner(scan);
		
		//iterate results
		Iterator<Result> itr = scanner.iterator();
		while (itr.hasNext()) {
			Result result = itr.next();
			
			String rowKeyString = Bytes.toString(result.getRow()); //getRow() returns the row key
			String dayString = Bytes.toString(result.getValue(FAMILY_NAME_BYTES, DAY_QUALIFIER_BYTES));
			long value = Bytes.toLong(result.getValue(FAMILY_NAME_BYTES, VALUE_QUALIFIER_BYTES));
			
			System.out.println("key = " + rowKeyString + "; day = '" + dayString + "'; value = " + value);
		}
	}

	private void dropAndCreateHbaseTable(Configuration conf) throws IOException {
		/*
		 * The HBase admin API let's you create, modify and delete tables.
		 */
		HBaseAdmin admin = new HBaseAdmin(conf);
		
		//Drop table, if it exists
		if (admin.tableExists(TABLE_NAME)) {
			admin.disableTable(TABLE_NAME);
			admin.deleteTable(TABLE_NAME);
		}
		
		//Create table
		HTableDescriptor desc = createTableDescriptor();
		admin.createTable(desc);
	}
	
	private HTableDescriptor createTableDescriptor() {
		HTableDescriptor result = new HTableDescriptor(TABLE_NAME);
		
		HColumnDescriptor family = new HColumnDescriptor(FAMILY_NAME);
		family.setMaxVersions(1);
		family.setTimeToLive(HConstants.FOREVER);
		
		result.addFamily(family);
		
		return result;
	}

	private HTable openTable(Configuration conf) throws IOException {
		return new HTable(conf, TABLE_NAME);
	}

	private void putData(HTable table) throws IOException {
		Random random = new Random();
		
		Calendar cal = new GregorianCalendar();
		
		for (int c = 0; c < 1000; c++) {
			cal.add(GregorianCalendar.HOUR, 24);
			Put put = createPut(random, cal.getTime());
			table.put(put);
		}
		
		table.flushCommits();
	}

	private Put createPut(Random random, Date day) {
		Put put = new Put(Bytes.toBytes(keyFormat.format(day)));
		put.add(FAMILY_NAME_BYTES, DAY_QUALIFIER_BYTES, Bytes.toBytes("This day is " + dayFormat.format(day)));
		put.add(FAMILY_NAME_BYTES, VALUE_QUALIFIER_BYTES, Bytes.toBytes(random.nextLong()));
		
		return put;
	}
}
