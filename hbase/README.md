# HBase
Basic HBase access and CRUD example. There is one class in this project. It contains a basic example for creating tables and doing inserts and gets / scans against HBase. The pom.xml contains the minimal dependencies for a HBase project.
