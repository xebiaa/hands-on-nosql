package com.xebia.nosql.riak;

import static org.junit.Assert.*;

import java.util.Collection;

import org.junit.Test;

import com.basho.riak.client.RiakBucketInfo;
import com.basho.riak.client.RiakClient;
import com.basho.riak.client.RiakObject;
import com.basho.riak.client.response.BucketResponse;
import com.basho.riak.client.response.FetchResponse;

public class SimpleRiakTest {
    
    private RiakClient riak = new RiakClient("http://localhost:8098/riak");
    
    @Test
    public void storeAndFetchMovie() {
        // http://www.imdbapi.com/?t=Finding+Nemo
        RiakObject o = new RiakObject("movies", "tt0266543");
        o.setContentType("text/plain");
        o.setValue("Finding Nemo");
        riak.store(o);
        
        FetchResponse r = riak.fetch("movies", "tt0266543");
        assertTrue(r.isSuccess());
        
        RiakObject o2 = r.getObject();
        assertEquals("tt0266543", o2.getKey());
        assertEquals("Finding Nemo", o2.getValue());
    }
    
    @Test
    public void deleteMovie() {
        riak.delete("movies", "tt0266543");
        
        FetchResponse r = riak.fetch("movies", "tt0266543");
        assertFalse(r.isSuccess());
    }
    
    @Test
    public void listMovies() {
        BucketResponse r = riak.listBucket("movies");
        assertTrue(r.isSuccess());
         
        RiakBucketInfo info = r.getBucketInfo();
        Collection<String> keys = info.getKeys();
        assertEquals(1, keys.size());
        // Why 1? Because deleting a key doesn't remove it from bucket.keys...
        // http://lists.basho.com/pipermail/riak-users_lists.basho.com/2011-May/004318.html
    }
    
    /**
     * There is no straightforward way to delete all entries. So we have to do it manually.
     * @param bucket bucket
     */
    @Test
    public void deleteAllMovies() {
        BucketResponse r = riak.listBucket("movies");
        if (r.isSuccess()) {
            RiakBucketInfo info = r.getBucketInfo();
            for(String key: info.getKeys()) {
                riak.delete("movies", key);
            }
        }
    }
}
