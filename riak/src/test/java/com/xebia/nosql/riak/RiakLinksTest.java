package com.xebia.nosql.riak;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.basho.riak.client.RiakClient;
import com.basho.riak.client.RiakLink;
import com.basho.riak.client.RiakObject;
import com.basho.riak.client.response.WalkResponse;

public class RiakLinksTest {
    
    private RiakClient riak = new RiakClient("http://localhost:8098/riak");
    
    @Before
    public void storeMovies() {
        add("movie", "tt0113189", "Golden Eye");
        add("movie", "tt0120347", "Tomorrow Never Dies");
        add("movie", "tt0143145", "The World Is Not Enough");
        
        RiakObject pierceBrosnan = add("actor", "nm0000112", "Pierce Brosnan");
        
        // add links
        List<RiakLink> links = pierceBrosnan.getLinks();
        links.add(new RiakLink("movie", "tt0113189", "plays-in"));
        links.add(new RiakLink("movie", "tt0120347", "plays-in"));
        links.add(new RiakLink("movie", "tt0143145", "plays-in"));
        riak.store(pierceBrosnan);
    }
    
    @Test
    public void shouldFindAllPierceBrosnanMovies() {
        WalkResponse r = riak.walk("actor", "nm0000112", "movie,_,1");
        assertTrue(r.isSuccess());
        List<? extends List<RiakObject>> steps = r.getSteps();
        assertEquals(1, steps.size());
        assertEquals(3, steps.get(0).size());
        
        for (List<RiakObject> step : steps) {
            for (RiakObject o : step) {
                System.out.println(o.getValue());
            }
        }
    }
   
    private RiakObject add(String bucket, String id, String name) {
        RiakObject o = new RiakObject(bucket, id);
        o.setContentType("text/plain");
        o.setValue(name);
        riak.store(o);
        return o;
    }
}
