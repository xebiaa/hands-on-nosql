## Neo4j

Most of this code is copied from the Neo4j examples: https://svn.neo4j.org/examples/imdb/trunk/

To create a new Neo4j database run DatabaseCreator.
This will create a new database in the directory: /tmp/nljug/neo4j.

The database will have 3 movies and 3 actors.

Have fun!
