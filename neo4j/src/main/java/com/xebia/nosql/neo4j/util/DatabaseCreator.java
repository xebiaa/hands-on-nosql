package com.xebia.nosql.neo4j.util;


import com.xebia.nosql.neo4j.domain.SimpleActor;
import com.xebia.nosql.neo4j.domain.SimpleMovie;
import com.xebia.nosql.neo4j.domain.SimpleRole;
import com.xebia.nosql.neo4j.service.ImdbService;
import com.xebia.nosql.neo4j.service.ImdbServiceImpl;
import org.apache.commons.io.FileUtils;
import org.neo4j.kernel.EmbeddedGraphDatabase;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DatabaseCreator {

    private ImdbService imdbService;

    public void createDatabase() throws IOException {
        File tempFile = new File("/tmp/nljug/neo4j");
        FileUtils.forceDelete(tempFile);
        tempFile.mkdir();
        imdbService = new ImdbServiceImpl(new EmbeddedGraphDatabase(tempFile.getAbsolutePath()));

        createActors();
        createMovies();
        createRoles();
    }

    private void createActors() {
        for (SimpleActor actor : getActors()) {
            imdbService.createActor(actor.getName());
        }
    }

    private void createMovies() {
        for (SimpleMovie movie : getMovies()) {
            imdbService.createMovie(movie.getName(), movie.getYear());
        }
    }

    private void createRoles() {
        for (SimpleRole role : getRoles()) {
            imdbService.createRole(imdbService.getActor(role.getActor()), imdbService.getMovie(role.getMovie()), role.getRole());
        }
    }

    private List<SimpleRole> getRoles() {
        List<SimpleRole> result = new ArrayList<SimpleRole>();

        result.add(new SimpleRole("Movie1", "Role1", "Actor1"));
        result.add(new SimpleRole("Movie2", "Role1", "Actor2"));
        result.add(new SimpleRole("Movie3", "Role1", "Actor1"));
        result.add(new SimpleRole("Movie3", "Role2", "Actor2"));
        result.add(new SimpleRole("Movie3", "Role3", "Actor3"));


        return result;
    }

    private List<SimpleMovie> getMovies() {
        List<SimpleMovie> result = new ArrayList<SimpleMovie>();

        result.add(new SimpleMovie("Movie1", 2001));
        result.add(new SimpleMovie("Movie2", 2002));
        result.add(new SimpleMovie("Movie3", 2003));

        return result;
    }

    private List<SimpleActor> getActors() {
        List<SimpleActor> result = new ArrayList<SimpleActor>();

        result.add(new SimpleActor("Actor1"));
        result.add(new SimpleActor("Actor2"));
        result.add(new SimpleActor("Actor3"));

        return result;
    }

    public static void main(String[] args) throws IOException {
        DatabaseCreator creator = new DatabaseCreator();
        creator.createDatabase();
    }
}
