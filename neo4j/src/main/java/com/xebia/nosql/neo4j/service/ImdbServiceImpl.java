package com.xebia.nosql.neo4j.service;

import com.xebia.nosql.neo4j.domain.*;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.graphdb.index.IndexManager;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ImdbServiceImpl implements ImdbService {
    private GraphDatabaseService graphDbService;
    private Index<Node> index;

    private static final String TITLE_INDEX = "title";
    private static final String NAME_INDEX = "name";


    public ImdbServiceImpl(GraphDatabaseService graphDatabaseService) {
        this.graphDbService = graphDatabaseService;
        IndexManager index = graphDbService.index();
        this.index = index.forNodes("NodeIdIndex");
    }

    public Actor createActor(final String name) {
        Actor actor = null;

        Transaction transaction = graphDbService.beginTx();
        try {
            final Node actorNode = graphDbService.createNode();
            actor = new ActorImpl(actorNode);
            actor.setName(name);
            index.add(actorNode, NAME_INDEX, name);
            transaction.success();
        } finally {
            transaction.finish();
        }
        return actor;
    }

    public Movie createMovie(final String title, final int year) {
        Movie movie = null;
        Transaction transaction = graphDbService.beginTx();
        try {
            final Node movieNode = graphDbService.createNode();
            movie = new MovieImpl(movieNode);
            movie.setTitle(title);
            movie.setYear(year);
            index.add(movieNode, TITLE_INDEX, title);
            transaction.success();
        } finally {
            transaction.finish();
        }
        return movie;
    }

    public Role createRole(final Actor actor, final Movie movie,
                           final String roleName) {
        Role role = null;
        Transaction transaction = graphDbService.beginTx();
        try {
            if (actor == null) {
                throw new IllegalArgumentException("Null actor");
            }
            if (movie == null) {
                throw new IllegalArgumentException("Null movie");
            }
            final Node actorNode = ((ActorImpl) actor).getUnderlyingNode();
            final Node movieNode = ((MovieImpl) movie).getUnderlyingNode();
            final Relationship rel = actorNode.createRelationshipTo(movieNode,
                    RelTypes.ACTS_IN);
            role = new RoleImpl(rel);
            if (roleName != null) {
                role.setName(roleName);
                transaction.success();
            }
        } finally {
            transaction.finish();
        }
        return role;
    }

    public Actor getActor(final String name) {
        Node actorNode = index.get(NAME_INDEX, name).getSingle();
        Actor actor = null;
        if (actorNode != null) {
            actor = new ActorImpl(actorNode);
        }
        return actor;
    }

    public Movie getMovie(final String title) {
        Node movieNode = getExactMovieNode(title);
        Movie movie = null;
        if (movieNode != null) {
            movie = new MovieImpl(movieNode);
        }
        return movie;
    }

    public Movie getExactMovie(final String title) {
        Node movieNode = getExactMovieNode(title);
        Movie movie = null;
        if (movieNode != null) {
            movie = new MovieImpl(movieNode);
        }
        return movie;
    }

    private Node getExactMovieNode(final String title) {
        Node movieNode = null;
        try {
            movieNode = index.get(TITLE_INDEX, title).getSingle();
        } catch (RuntimeException e) {
            System.out.println("Duplicate index for movie title: " + title);
            Iterator<Node> movieNodes = index.get(TITLE_INDEX,
                    title).iterator();
            if (movieNodes.hasNext()) {
                movieNode = movieNodes.next();
            }
        }
        return movieNode;
    }

    public void setupReferenceRelationship() {
        Node baconNode = index.get("name", "Bacon, Kevin").getSingle();
        if (baconNode == null) {
            throw new NoSuchElementException(
                    "Unable to find Kevin Bacon actor");
        }
        Node referenceNode = graphDbService.getReferenceNode();
        referenceNode.createRelationshipTo(baconNode, RelTypes.IMDB);
    }

}
