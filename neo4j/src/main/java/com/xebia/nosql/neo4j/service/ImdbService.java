package com.xebia.nosql.neo4j.service;

import com.xebia.nosql.neo4j.domain.Actor;
import com.xebia.nosql.neo4j.domain.Movie;
import com.xebia.nosql.neo4j.domain.Role;

public interface ImdbService {
    /**
     * Store a new actor in the graph and add the name to the index.
     *
     * @param name
     * @return the new actor
     */
    Actor createActor(String name);

    /**
     * Store a new movie and add the title to the index.
     *
     * @param title title of the movie
     * @param year  year of release
     * @return the new movie
     */
    Movie createMovie(String title, int year);

    /**
     * Store a new role in the graph.
     *
     * @param actor    the actor
     * @param movie    the movie
     * @param roleName name of the role
     * @return the new role
     */
    Role createRole(Actor actor, Movie movie, String roleName);

    /**
     * Returns the actor with the given <code>name</code> or <code>null</code>
     * if not found.
     *
     * @param name name of actor
     * @return actor or <code>null</code> if not found
     */
    Actor getActor(String name);

    /**
     * Return the movie with given <code>title</code> or <code>null</code> if
     * not found.
     *
     * @param title movie title
     * @return movie or <code>null</code> if not found
     */
    Movie getMovie(String title);

    Movie getExactMovie(String title);

    /**
     * Add a relationship from some node to the reference node.
     * Will make it easy and fast to retrieve this node.
     */
    void setupReferenceRelationship();
}
