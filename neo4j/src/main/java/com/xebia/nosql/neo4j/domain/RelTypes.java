package com.xebia.nosql.neo4j.domain;

import org.neo4j.graphdb.RelationshipType;

public enum RelTypes implements RelationshipType {
    ACTS_IN, IMDB
}