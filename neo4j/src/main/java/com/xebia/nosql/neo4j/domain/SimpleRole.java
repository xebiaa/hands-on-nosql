package com.xebia.nosql.neo4j.domain;

public class SimpleRole {
    private final String movie;
    private final String role;
    private final String actor;

    public SimpleRole(final String movie, final String role, String actor) {
        this.movie = movie;
        this.role = role;
        this.actor = actor;
    }

    /**
     * Returns the movie of the movie, never <code>null</code>.
     *
     * @return title of the movie
     */
    public String getMovie() {
        return this.movie;
    }

    /**
     * Returns the role the actor had in the movie, may be <code>null</code>
     * if no information is available.
     *
     * @return actor role or null if information not avilable
     */
    public String getRole() {
        return this.role;
    }

    public String getActor() {
        return actor;
    }
}
