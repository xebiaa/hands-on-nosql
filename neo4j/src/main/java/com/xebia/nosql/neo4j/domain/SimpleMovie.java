package com.xebia.nosql.neo4j.domain;

public class SimpleMovie {
    private String name;
    private int year;

    public SimpleMovie(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }
}
