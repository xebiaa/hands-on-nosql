package com.xebia.nosql.neo4j.domain;

public class SimpleActor {

    private String name;


    public SimpleActor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
