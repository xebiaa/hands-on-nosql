package com.xebia.nosql.parse;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParseSamplesTest {
    @Test
    public void shouldLoadFileFromClassPath() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(ParseSamplesTest.class.getResourceAsStream("/sample.json")));

        int lineCount = 0;
        while (reader.readLine() != null) {
            lineCount++;
        }

        assertEquals(1, lineCount);
    }

    private String readSingleLineFromFile() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(ParseSamplesTest.class.getResourceAsStream("/sample.json")));
        String result = reader.readLine();
        reader.close();
        return result;
    }

    @SuppressWarnings("rawtypes")
    @Test
    public void shouldParseArrayAsList() throws IOException {
        String line = readSingleLineFromFile();

        List list = new ObjectMapper().readValue(line, List.class);

        assertEquals(20, list.size());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldParseAsListOfMaps() throws IOException {
        String line = readSingleLineFromFile();

        List<Map<String, Object>> list = (List<Map<String, Object>>) new ObjectMapper().readValue(line, List.class);

        assertTrue(Map.class.isAssignableFrom(list.get(0).getClass()));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldParseTwitterStatusFields() throws IOException {
        String line = readSingleLineFromFile();

        Map<String, Object> tweet = ((List<Map<String, Object>>) new ObjectMapper().readValue(line, List.class)).get(0);

        //fields at highest level
        assertEquals(tweet.get("id"), 128901902476910593L);
        assertEquals(tweet.get("text"), "@par01 yo danno!");

        //nested objects require an additional cast
        assertEquals(((Map<String, Object>) tweet.get("user")).get("screen_name"), "ThaFreeze");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldParseDate() throws IOException, ParseException {
        DateFormat format = new SimpleDateFormat("EEE MMM dd H:m:s Z y", Locale.ENGLISH); //e.g. Fri Feb 25 14:04:46 +0000 2011

        String line = readSingleLineFromFile();

        Map<String, Object> tweet = ((List<Map<String, Object>>) new ObjectMapper().readValue(line, List.class)).get(0);
        Date tweetDate = format.parse((String) tweet.get("created_at"));

        assertEquals(1319567583000L, tweetDate.getTime());

    }
}
