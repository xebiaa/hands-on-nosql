# Hands on NoSQL

## Dataset

The provided data consists of (almost) all @nljug followers with their most recent tweets.

Directories with .json files:
  followers/ - All followers user_ids for @nljug.
  statuses/ - Most recent status updates (tweets) for each user_id.
  user/ - User detailed information per user_id.

Relevant Twitter API:

- https://dev.twitter.com/docs/api/1/get/followers/ids
- https://dev.twitter.com/docs/api/1/get/statuses/user_timeline
- https://dev.twitter.com/docs/api/1/get/users/show

## Exercises

Here is a list of exercise suggestions. Discuss with your peers and pick some interesting ones.
Some might be more feasable for a certain NoSQL database than others.

- Load all tweets
- Load all users
- Give the tweet with id X.
- Give all tweets for user Y (by id and screen_name).
- Give all users that reply to tweet with id X.
- Give all tweets for a certain hashtag.
- Give all tweets between date X and Y.
- Give all tweets between date X and Y, that contain the hashtag Z.
- Give all twitter reply threads.
- Give all twitter reply threads, between date X and Y.

## Twitter Parser

A trivial Jackson based JSON parse samples can be found under parse-sample/

## Neo4j

See neo4j/README.md

## Riak

See riak/README.md

## MongoDB

See mongodb/README.md

Hint: Use an index on created_at and entities.hashtags.text

## HBase

See hbase/README.md

Hint: Use table scan
